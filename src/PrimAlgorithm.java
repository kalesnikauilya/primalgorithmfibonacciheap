import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PrimAlgorithm {
	private ArrayList<GraphState> graphStates = new ArrayList<GraphState>();
	private ArrayList<FibHeapState> fibHeapStates = new ArrayList<FibHeapState>();
	
	public ArrayList<GraphState> getGraphStates(){
		return graphStates;
	}
	
	public ArrayList<FibHeapState> getFibHeapStates(){
		return fibHeapStates;
	}
	
	public Graph mstPrim(Graph graph){
		
		//////
		GraphState graphState1 = new GraphState("Input graph:");
		for(Node node : graph.getNodes()){
			//System.out.println(node.getX() + " " + node.getY() + " " + node.getId());
			graphState1.addNodeImage(new GraphNodeImage(node.getX(), node.getY(), node.getId(), Color.green));
			//System.out.println(graphState1.graphNodeImages.size());
		}
		for(Edge edge : graph.getEdges()){
			Node n1 = edge.getNode1();
			Node n2 = edge.getNode2();
			graphState1.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), edge.getCost(), Color.green));
		}
		graphStates.add(graphState1);
		
		fibHeapStates.add(new FibHeapState("Emtpy heap"));
		//////
		
		
		FibonacciHeap<Node> fibonacciHeap = new FibonacciHeap<Node>(graphStates, fibHeapStates);
		
		//mapping from graph nodes to its entries in fibonacciHeap
		Map<Node, FibHeapEntry<Node>> entriesMap = new HashMap<Node, FibHeapEntry<Node>>();
	
		Graph result = new Graph();
		
		if(graph.isEmpty()){
			return result;
		}
		
		//select start node and add all his neighbours to fibheap
		Random random = new Random();
		Node startNode = graph.getNodeById(random.nextInt(graph.size()) + 1);
		result.addNode(startNode);
		//result.addNode(graph.getNodeById(4));
		
		
		//////
		GraphState graphState2 = new GraphState("Starting from node " + startNode.getId());
		for(Node node : graph.getNodes()){
			if(result.contansNode(node)){
				graphState2.addNodeImage(new GraphNodeImage(node.getX(), node.getY(), node.getId(), Color.red));
			}else{
				graphState2.addNodeImage(new GraphNodeImage(node.getX(), node.getY(), node.getId(), Color.green));
			}
		}
		for(Edge edge : graph.getEdges()){
			Node n1 = edge.getNode1();
			Node n2 = edge.getNode2();
			graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), edge.getCost(), Color.green));
		}
		graphStates.add(graphState2);
		fibHeapStates.add(new FibHeapState("Emtpy heap"));
		//////
		
		
		addAdjacentNodes(startNode, graph, fibonacciHeap, result, entriesMap);
	
		//add n-1 edges to result
		for(int i=0; i<graph.size()-1; i++){
			
			
			//////
			GraphState graphState4 = new GraphState("Considered all edges");
			for(Node nn : graph.getNodes()){
				if(result.contansNode(nn)){
					graphState4.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
				}else{
					graphState4.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
				}
			}
			for(Edge ee : graph.getEdges()){
				Node n1 = ee.getNode1();
				Node n2 = ee.getNode2();
				if(result.containsEdge(n1, n2)){
					graphState4.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
				}else{
					graphState4.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
				}
			}
			graphStates.add(graphState4);
			fibHeapStates.add(fibonacciHeap.getFibHeapState(""));
			//////						
			
			
			
			//get from fibheap best node
			Node bestNode = fibonacciHeap.extractMin().getValue();
			
			//determine which edge to add
			Node adjNode = minCostAdjNode(bestNode, graph, result);
			
			result.addNode(bestNode);
			result.addEdge(bestNode,  adjNode, graph.edgeCost(bestNode,  adjNode));
		
			
			//////
			GraphState graphState3 = new GraphState("added node " + bestNode.getId() + " and edge {" + bestNode.getId() + ", " + adjNode.getId() + "} to temporary MST");
			for(Node nn : graph.getNodes()){
				if(result.contansNode(nn)){
					graphState3.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
				}else{
					graphState3.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
				}
			}
			for(Edge ee : graph.getEdges()){
				Node n1 = ee.getNode1();
				Node n2 = ee.getNode2();
				if(result.containsEdge(n1, n2)){
					graphState3.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
				}else{
					graphState3.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
				}
			}
			graphStates.add(graphState3);
			fibHeapStates.add(fibHeapStates.get(fibHeapStates.size()-1));
			//////			
			
			
			
			if(i==graph.size()-2){
				break;
			}
			addAdjacentNodes(bestNode, graph, fibonacciHeap, result, entriesMap);
		}
		
		//////
		GraphState graphState3 = new GraphState("Result MST");
		for(Node nn : graph.getNodes()){
			if(result.contansNode(nn)){
				graphState3.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
			}else{
				graphState3.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
			}
		}
		for(Edge ee : graph.getEdges()){
			Node n1 = ee.getNode1();
			Node n2 = ee.getNode2();
			if(result.containsEdge(n1, n2)){
				graphState3.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
			}else{
				graphState3.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
			}
		}
		graphStates.add(graphState3);
		fibHeapStates.add(fibonacciHeap.getFibHeapState(""));
		//////				
		
		
		return result;
	}
	
	//get best node adjacent to given node
	private Node minCostAdjNode(Node node, Graph graph, Graph result){
		Node cheapestNode = null;
		int cheapestCost = Integer.MAX_VALUE;
		for(Map.Entry<Node, Integer> entry : graph.edgesFrom(node).entrySet()){
			if(!result.contansNode(entry.getKey())) continue;
			if(entry.getValue() >= cheapestCost) continue;
			cheapestNode = entry.getKey();
			cheapestCost = entry.getValue();
		}
		
		return cheapestNode;
	}
	
	//add adjacent nodes to fibHeap or update their priorities
	private void addAdjacentNodes(Node node, Graph gr, FibonacciHeap<Node> fh, Graph res, Map<Node, FibHeapEntry<Node>> map){
		//for all adj edges
		for(Map.Entry<Node, Integer> edge : gr.edgesFrom(node).entrySet()){
			
			
			//node is in result
			if(res.contansNode(edge.getKey())){				
				
				//////
				GraphState graphState2 = new GraphState("Node " + ((Node)edge.getKey()).getId() + " is already in temporary MST");
				for(Node nn : gr.getNodes()){
					if(res.contansNode(nn)){
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
					}else{
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
					}
				}
				for(Edge ee : gr.getEdges()){
					Node n1 = ee.getNode1();
					Node n2 = ee.getNode2();
					if( (n1.getId() == node.getId() && n2.getId() == edge.getKey().getId()) || 
					    (n2.getId() == node.getId() && n1.getId() == edge.getKey().getId())){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.blue));
					}else if(res.containsEdge(n1, n2)){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
					}else{
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
					}
				}
				graphStates.add(graphState2);
				//////		
				
				fibHeapStates.add(fh.getFibHeapState(""));
				continue;
			}
			
			//if node is not in fibheap, else if there is better edge
			if(!map.containsKey(edge.getKey())){
				
				//////
				GraphState graphState2 = new GraphState("Adding node " + ((Node)edge.getKey()).getId() + " to the fib heap");
				for(Node nn : gr.getNodes()){
					if(res.contansNode(nn)){
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
					}else{
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
					}
				}
				for(Edge ee : gr.getEdges()){
					Node n1 = ee.getNode1();
					Node n2 = ee.getNode2();
					if( (n1.getId() == node.getId() && n2.getId() == edge.getKey().getId()) || 
					    (n2.getId() == node.getId() && n1.getId() == edge.getKey().getId())){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.blue));
					}else if(res.containsEdge(n1, n2)){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
					}else{
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
					}
				}
				graphStates.add(graphState2);
				//////
				
				map.put(edge.getKey(), fh.insert(edge.getKey(), edge.getValue()));
			}else if(map.get(edge.getKey()).priority > edge.getValue()){
				
				//////
				GraphState graphState2 = new GraphState("Update priority of node " + ((Node)edge.getKey()).getId());
				for(Node nn : gr.getNodes()){
					if(res.contansNode(nn)){
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
					}else{
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
					}
				}
				for(Edge ee : gr.getEdges()){
					Node n1 = ee.getNode1();
					Node n2 = ee.getNode2();
					if( (n1.getId() == node.getId() && n2.getId() == edge.getKey().getId()) || 
					    (n2.getId() == node.getId() && n1.getId() == edge.getKey().getId())){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.blue));
					}else if(res.containsEdge(n1, n2)){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
					}else{
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
					}
				}
				graphStates.add(graphState2);
				//////
				
				fh.decreaseKey(map.get(edge.getKey()), edge.getValue());
			}else{
				//////
				GraphState graphState2 = new GraphState("Node " + ((Node)edge.getKey()).getId() + " is in fib heap and has smaller priority");
				for(Node nn : gr.getNodes()){
					if(res.contansNode(nn)){
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.red));
					}else{
						graphState2.addNodeImage(new GraphNodeImage(nn.getX(), nn.getY(), nn.getId(), Color.green));
					}
				}
				for(Edge ee : gr.getEdges()){
					Node n1 = ee.getNode1();
					Node n2 = ee.getNode2();
					if( (n1.getId() == node.getId() && n2.getId() == edge.getKey().getId()) || 
					    (n2.getId() == node.getId() && n1.getId() == edge.getKey().getId())){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.blue));
					}else if(res.containsEdge(n1, n2)){
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.red));
					}else{
						graphState2.addEdgeImage(new GraphEdgeImage(n1.getX(), n1.getY(), n2.getX(), n2.getY(), ee.getCost(), Color.green));
					}
				}
				graphStates.add(graphState2);
				//////
				
				
				fibHeapStates.add(fh.getFibHeapState("Node " + edge.getKey().getId() + " is inserted in fib heap with smaller priority"));
			}
		}
		
		//System.out.println("fibheap size: " + fh.getSize());
	}
}
