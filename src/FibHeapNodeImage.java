import java.awt.Color;

public class FibHeapNodeImage {
	public int x;
	public int y;
	public int nodeId;
	public int priority;
	public Color color;
	public boolean mark;
	
	public FibHeapNodeImage(int xx, int yy, int nid, int pr, Color c, boolean m){
		x=xx;
		y=yy;
		nodeId=nid;
		priority=pr;
		color=c;
		mark = m;
	}
}
