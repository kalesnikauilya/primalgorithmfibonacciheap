import java.awt.Color;

public class GraphEdgeImage {
	public int x1;
	public int y1;
	public int x2;
	public int y2;
	public int length;
	public Color color;
	
	public GraphEdgeImage(int xx1, int yy1, int xx2, int yy2, int len, Color cc){
		x1=xx1;
		y1=yy1;
		x2=xx2;
		y2=yy2;
		length=len;
		color=cc;
	}
}
