import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

public class GraphState extends JComponent{
	private static final long serialVersionUID = 1L;

	private int nodeRadius = 30;
	private int maxX = 0;
	private int maxY = 0;
	
	public ArrayList<GraphNodeImage> graphNodeImages = new ArrayList<GraphNodeImage>();
	public ArrayList<GraphEdgeImage> graphEdgeImages = new ArrayList<GraphEdgeImage>();
	
	private String commentary;
	
	public String getCommentary(){
		return commentary;
	}
	
	public void addNodeImage(GraphNodeImage gni){
		graphNodeImages.add(gni);
		maxX=Math.max(maxX, gni.x);
		maxY=Math.max(maxY, gni.y);
	}
	
	public void addEdgeImage(GraphEdgeImage gei){
		graphEdgeImages.add(gei);
	}
	
	public GraphState(String c) {
		this.setBorder(BorderFactory.createRaisedBevelBorder());
		commentary = c;
        //setPreferredSize(new Dimension(500, 100));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
   	 	this.setPreferredSize(new Dimension(maxX+nodeRadius, maxY+nodeRadius));
        g.setColor(Color.white);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        for(GraphEdgeImage gei : graphEdgeImages){
        	g.setColor(gei.color);
        	g.drawLine(gei.x1, gei.y1, gei.x2, gei.y2);
        	g.setColor(Color.black);
        	g.drawString(Integer.toString(gei.length), (gei.x1+gei.x2)/2, (gei.y1+gei.y2)/2);
        }
        
        for(GraphNodeImage gni : graphNodeImages){
        	g.setColor(gni.color);
        	g.fillOval(gni.x-nodeRadius/2, gni.y-nodeRadius/2, nodeRadius, nodeRadius);
        	g.setColor(Color.black);
        	g.drawString(Integer.toString(gni.id), gni.x, gni.y);
        }
        
        //g.setColor(Color.red);
        //g.fillRect(200, 62, 30, 10);
    }
}
