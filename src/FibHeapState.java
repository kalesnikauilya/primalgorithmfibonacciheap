import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;

public class FibHeapState extends JComponent{
	private static final long serialVersionUID = 1L;

	private int nodeRadius = 30;
	private int maxX = 0;
	private int maxY = 0;
	
	public ArrayList<FibHeapNodeImage> fibHeapNodeImages = new ArrayList<FibHeapNodeImage>();
	public ArrayList<FibHeapEdgeImage> fibHeapEdgeImages = new ArrayList<FibHeapEdgeImage>();
	
	private String commentary;
	
	public String getCommentary(){
		return commentary;
	}
	
	public void addNodeImage(FibHeapNodeImage fhni){
		fibHeapNodeImages.add(fhni);
		maxX=Math.max(maxX, fhni.x);
		maxY=Math.max(maxY, fhni.y);
	}
	
	public void addEdgeImage(FibHeapEdgeImage fhei){
		fibHeapEdgeImages.add(fhei);
	}
	
	public void changeNodeColor(int id, Color c){
		for(FibHeapNodeImage fhni : fibHeapNodeImages){
			if(fhni.nodeId==id){
				fhni.color = c;
				return;
			}
		}
	}
	
	public FibHeapState(String c) {
		commentary = c;
        //setPreferredSize(new Dimension(500, 100));
    }

    @Override
    public void paintComponent(Graphics g) {
    	 super.paintComponent(g);
    	 this.setPreferredSize(new Dimension(maxX+nodeRadius, maxY+nodeRadius));
    	 g.setColor(Color.white);
         g.fillRect(0, 0, this.getWidth(), this.getHeight());
         
         
         
         for(FibHeapEdgeImage fhei : fibHeapEdgeImages){
         	g.setColor(fhei.color);
         	g.drawLine(fhei.x1, fhei.y1, fhei.x2, fhei.y2);
         }
         
         for(FibHeapNodeImage fhni : fibHeapNodeImages){
         	g.setColor(fhni.color);
         	g.fillOval(fhni.x-nodeRadius/2, fhni.y-nodeRadius/2, nodeRadius, nodeRadius);
         	g.setColor(Color.black);
         	g.drawString(Integer.toString(fhni.nodeId) + "(" + Integer.toString(fhni.priority) + ")", fhni.x-nodeRadius/3, fhni.y);
         	if(fhni.mark){
         		g.drawString("M", fhni.x-nodeRadius/8, fhni.y+nodeRadius/2);
         	}
         }
    }
}
