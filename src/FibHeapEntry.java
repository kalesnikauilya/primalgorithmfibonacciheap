
//class represents single node in fibonacci heap
public class FibHeapEntry<T> {
	public int degree = 0;
	public boolean mark = false;
	
	public FibHeapEntry<T> left;
	public FibHeapEntry<T> right;
	public FibHeapEntry<T> parent;
	public FibHeapEntry<T> child;
	
	private T element;
	public int priority; 
	
	public int x;
	public int y;
	
	public FibHeapEntry(T e, int p){
		left = this;
		right = this;
		element = e;
		priority = p;
	}
	
	public T getValue(){
		return element;
	}
}
