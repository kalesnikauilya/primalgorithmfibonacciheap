
public class Edge {
	private Node node1;
	private Node node2;
	private int cost;
	
	public Edge(Node n1, Node n2, int c){
		node1=n1;
		node2=n2;
		cost=c;
	}
	
	public Node getNode1(){
		return node1;
	}
	
	public Node getNode2(){
		return node2;
	}
	
	public int getCost(){
		return cost;
	}
}
