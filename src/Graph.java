import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

public class Graph {
	private Map<Node, Map<Node, Integer>> adj = new HashMap<Node, Map<Node, Integer>>();
	private ArrayList<Edge> edges = new ArrayList<Edge>();
	
	
	private Set<Node> visited = new HashSet<Node>();
	private void dfs(Node n){
		visited.add(n);
		for(Node to : edgesFrom(n).keySet()){
			if(!visited.contains(to)){
				dfs(to);
			}
		}
	}
	
	public boolean isConnected(){
		visited.clear();
		if(adj.isEmpty()){
			return true;
		}
		dfs(adj.entrySet().iterator().next().getKey());
		if(visited.size() != adj.size()){
			return false;
		}
		return true;
	}
	
	public ArrayList<Edge> getEdges(){
		return edges;
	}
	
	public Set<Node> getNodes(){
		return adj.keySet();
	}
	
	//get node by its id
	public Node getNodeById(int id){
		for(Node n : adj.keySet()){
			if(n.getId()==id){
				return n;
			}
		}
		return null;
	}
	
	//add node to graph, if such node already exists, return false
	public boolean addNode(Node n){
		if(adj.containsKey(n)){
			return false;
		}
		adj.put(n, new HashMap<Node, Integer>());
		return true;
	}
	
	//add new edge to graph 
	public void addEdge(Node n1, Node n2, int edgeCost){
		//System.out.println("addEdge: {"  + n1.getId() + " " + n2.getId() + "} - " + edgeCost);
		adj.get(n1).put(n2, edgeCost);
		adj.get(n2).put(n1, edgeCost);
		
		edges.add(new Edge(n1, n2, edgeCost));
	}
	
	// get all edges outgoing from given node
	public Map<Node, Integer> edgesFrom(Node n){
		Map<Node, Integer> edges = adj.get(n);
		return Collections.unmodifiableMap(edges);
	}
	
	//check if graph contains given node
	public boolean contansNode(Node n){
		return adj.containsKey(n);
	}
	
	//check if graph contains edge between n1 and n2
	public boolean containsEdge(Node n1, Node n2){
		if(!adj.containsKey(n1) || !adj.containsKey(n2)) return false;
		return edgesFrom(n1).get(n2) != null;
	}
	
	//return edge cost from n1 to n2
	public int edgeCost(Node n1, Node n2){
		//System.out.println("getEdgeCost: {"  + n1.getId() + " " + n2.getId() + "}");
		return adj.get(n1).get(n2);
	}
	
	@SuppressWarnings("resource")
	public void readGraphFromFile(String fileName) throws InputMismatchException, FileNotFoundException, NoSuchElementException{
		Scanner scanner;
	
		scanner = new Scanner(new File(fileName));
		int n = scanner.nextInt();
			
		for(int i=1; i<=n; i++){
			int nodeId = i;
			int nodeX = scanner.nextInt();
			int nodeY = scanner.nextInt();
			@SuppressWarnings("unused")
			int nodeZ = scanner.nextInt();
			this.addNode(new Node(nodeId, nodeX, nodeY));
		}
			
		for(int i=1; i<=n; i++){
			for(int j=1; j<=n; j++){
				int edgeCost = scanner.nextInt();
				if(edgeCost==-1) continue;
				if(i<j) continue; //dont add twice
				Node n1 = this.getNodeById(i);
				Node n2 = this.getNodeById(j);
				
				this.addEdge(n1, n2, edgeCost);
			}
		}
	}
	
	public int size(){
		return adj.size();
	}
	
	public boolean isEmpty() {
		return adj.isEmpty();
	}
	
	public void printGraph(){
		for(Map.Entry<Node, Map<Node, Integer>> entry : adj.entrySet()){
			System.out.println(entry.getKey().getId() + ":");
			for(Entry<Node, Integer> edge : entry.getValue().entrySet()){
				System.out.println("{" + entry.getKey().getId() + " " + edge.getKey().getId() + "} - " + edge.getValue());
			}
		}
	}
}
