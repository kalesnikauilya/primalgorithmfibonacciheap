import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class FibonacciHeap<T> {
	private FibHeapEntry<T> minEntry = null;
	private int size = 0;
	
	private ArrayList<GraphState> graphStates;
	private ArrayList<FibHeapState> fibHeapStates;
	
	public FibonacciHeap (ArrayList<GraphState> gs, ArrayList<FibHeapState> fhs){
		graphStates = gs;
		fibHeapStates = fhs;
	}
	
	public FibHeapEntry<T> getMin(){
		return minEntry;
	}
	
	public int getSize(){
		return size;
	}
	
	//return minEntry and delete it from tree
	public FibHeapEntry<T> extractMin(){
		FibHeapState fhState = getFibHeapState("Extract min " + ((Node)minEntry.getValue()).getId());
		fhState.changeNodeColor(((Node)minEntry.getValue()).getId(), Color.LIGHT_GRAY);
		fibHeapStates.add(fhState);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		
		size--;
		FibHeapEntry<T> result = minEntry;
		
		//delete minEntry form list of roots
		if(minEntry.right == minEntry){
			minEntry = null;
		}else{
			minEntry.left.right = minEntry.right;
			minEntry.right.left = minEntry.left;
			minEntry = minEntry.right; //any entry 
		}
		
		//for all children of minEntry clear parent pointer
		if(result.child != null){
			FibHeapEntry<T> cur = result.child;
			do{
				cur.parent = null;
				cur = cur.right;
			}while(cur != result.child);
		}
	
		//merge all children of minEntry to root list
		minEntry = mergeLists(minEntry, result.child);
		
		if(minEntry == null){
			FibHeapState fhState3 = getFibHeapState("Extract min " + ((Node)result.getValue()).getId() + " : done");
			fibHeapStates.add(fhState3);
			while(graphStates.size()<fibHeapStates.size()){
				graphStates.add(graphStates.get(graphStates.size()-1));
			}
			return result;
		}
		
		//array of tree degrees
		List<FibHeapEntry<T>> treeTable = new ArrayList<FibHeapEntry<T>>();
		List<FibHeapEntry<T>> toVisit = new ArrayList<FibHeapEntry<T>>();
		for(FibHeapEntry<T> cur = minEntry; toVisit.isEmpty() || toVisit.get(0) != cur; cur = cur.right){
			toVisit.add(cur);
		}
		
		//get rid of trees with same degree
		for(FibHeapEntry<T> cur : toVisit){
			while(true){	
				while(cur.degree >= treeTable.size()){
					treeTable.add(null);
				}
				
				//if there is no tree with cur.degree
				if(treeTable.get(cur.degree) == null){
					treeTable.set(cur.degree, cur);
					break;
				}
				

				
				//get another tree of this degree
				FibHeapEntry<T> copy = treeTable.get(cur.degree);
				treeTable.set(cur.degree, null);
				
				
				FibHeapState fhState5 = getFibHeapState("Extract min " + ((Node)result.getValue()).getId() + " : subtree " + ((Node)cur.getValue()).getId() + " and " + ((Node)copy.getValue()).getId() + " have the same degree");
				fibHeapStates.add(fhState5);
				while(graphStates.size()<fibHeapStates.size()){
					graphStates.add(graphStates.get(graphStates.size()-1));
				}
				
				//which of cur and copy is min/max
				FibHeapEntry<T> min;
				FibHeapEntry<T> max;
				if(copy.priority < cur.priority){
					min = copy;
					max = cur;
				}else{
					min = cur;
					max = copy;
				}
				
				//delete max from root list
				max.right.left = max.left;
				max.left.right = max.right;
				
				//merge max to min as child
				max.right = max;
				max.left = max;
				min.child = mergeLists(min.child, max);
				max.parent = min;
				
				//unmark max
				max.mark = false;
				
				min.degree++;
				
				cur = min;
				
				
				if(cur.priority <= minEntry.priority){
					minEntry = cur;
				}
				
				
				FibHeapState fhState1 = getFibHeapState("Extract min " + ((Node)result.getValue()).getId() + " : connecting two subtrees with same degree");
				fibHeapStates.add(fhState1);
				while(graphStates.size()<fibHeapStates.size()){
					graphStates.add(graphStates.get(graphStates.size()-1));
				}
			}
			if(cur.priority <= minEntry.priority){
				minEntry = cur;
			}
		}
		
		FibHeapState fhState3 = getFibHeapState("Extract min " + ((Node)result.getValue()).getId() + " : done");
		fibHeapStates.add(fhState3);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		
		return result;
	}
	
	// merge two lists of entries and return minimum entry
	private FibHeapEntry<T> mergeLists(FibHeapEntry<T> entry1, FibHeapEntry<T> entry2){
		if(entry1 == null && entry2 == null){
			return null;
		}else if(entry1 != null && entry2 == null){
			return entry1;
		}else if(entry1 == null && entry2 != null){
			return entry2;
		}else{
			FibHeapEntry<T> temp = entry1.right;
			entry1.right = entry2.right;
			entry1.right.left = entry1;
			entry2.right = temp;
			entry2.right.left = entry2;
			
			return entry1.priority < entry2.priority ? entry1 : entry2;
		}
	}
	
	public FibHeapEntry<T> insert(T elem, int prior){
		FibHeapEntry<T> res = new FibHeapEntry<T>(elem, prior);
		minEntry = mergeLists(minEntry, res);
		size++;
		
		FibHeapState fhState = getFibHeapState("Insert: " + ((Node)elem).getId());
		fhState.changeNodeColor(((Node)elem).getId(), Color.ORANGE);
		fibHeapStates.add(fhState);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		return res;
	}
	
	private void cut(FibHeapEntry<T> entry){		

		
		entry.mark=false;

		//if entry is root
		if(entry.parent == null) return;
		
		//change pointers of right and left nodes of entry
		if(entry.right != entry){
			entry.right.left = entry.left;
			entry.left.right = entry.right;
		}
		
		//if entry was a pointer to his parents child, change this pointer
		if(entry.parent.child == entry){
			if(entry.right != entry){
				entry.parent.child = entry.right;
			}else{
				entry.parent.child = null;
			}
		}
		
		entry.parent.degree--;
		
		//add entry into root list
		entry.left = entry;
		entry.right = entry;
		minEntry = mergeLists(minEntry, entry);
		
		//recursively cut
		if(entry.parent.mark){
			cut(entry.parent);
		}else{
			entry.parent.mark = true;
		}
		
		entry.parent = null;
		
		/////
		FibHeapState fhState1 = getFibHeapState("Decrease key: cut node " + ((Node)entry.getValue()).getId());
		fibHeapStates.add(fhState1);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		/////
	}
	
	public void decreaseKey(FibHeapEntry<T> entry, int priority){
		entry.priority = priority;
		
		/////
		FibHeapState fhState1 = getFibHeapState("Decrease key: decrese priority of node " + ((Node)entry.getValue()).getId() + " to " + priority);
		fibHeapStates.add(fhState1);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		/////
		
		if(entry.parent != null && entry.priority <= entry.parent.priority){
			cut(entry);
		}
		
		if(entry.priority <= minEntry.priority){
			minEntry = entry;
		}
		
		/////
		FibHeapState fhState2 = getFibHeapState("Decrease key: done");
		fibHeapStates.add(fhState2);
		while(graphStates.size()<fibHeapStates.size()){
			graphStates.add(graphStates.get(graphStates.size()-1));
		}
		/////
	}
	
	
	
	
	//used only in getFibHeapState() method 
		@SuppressWarnings("hiding")
		private class StackEntry<T>{
			public FibHeapEntry<T> fibHeapEntry;
			public int level;
			public StackEntry (FibHeapEntry<T> fhe, int lvl){
				fibHeapEntry = fhe;
				level = lvl;
			}
		}
		
		//used only in getFibHeapState()
		//recursive adding all lines
		private void addLines(FibHeapEntry<T> curEntry, FibHeapState res, boolean b){
			if(curEntry == null) return;
			if(curEntry.right != curEntry && b){
				FibHeapEntry<T> firstEntry = curEntry;
				FibHeapEntry<T> secondEntry = curEntry.right;
				while(secondEntry!=curEntry){
					res.addEdgeImage(new FibHeapEdgeImage(firstEntry.x, firstEntry.y, secondEntry.x, secondEntry.y, ((Node)firstEntry.getValue()).getId(), ((Node)secondEntry.getValue()).getId(), Color.BLUE));
					addLines(secondEntry, res, false);
					firstEntry = firstEntry.right;
					secondEntry = secondEntry.right;
				}
			}
			
			if(curEntry.child != null){
				res.addEdgeImage(new FibHeapEdgeImage(curEntry.x, curEntry.y, curEntry.child.x, curEntry.child.y, ((Node)curEntry.getValue()).getId(), ((Node)curEntry.child.getValue()).getId(), Color.BLUE));
				FibHeapEntry<T> tempEntry = curEntry.child;
				do{
					addLines(tempEntry, res, true);
					tempEntry = tempEntry.right;
				}while(tempEntry!=curEntry.child);
			}
		}
		
		public FibHeapState getFibHeapState(String c){
			int dx = 50;
			int dy = 50;
			FibHeapState res = new FibHeapState(c);
			if(minEntry==null){
				return res;
			}
			
			Stack<StackEntry<T>> stack = new Stack<StackEntry<T>>();
			int rightmostFree = 1;
			
			FibHeapEntry<T> curEntry = minEntry;
			while(true){
				curEntry = curEntry.left;
				stack.push(new StackEntry<T>(curEntry, 1));
				if(curEntry==minEntry) break;
			}
			
			StackEntry<T> previousPoped = null;
			while(!stack.empty()){
				StackEntry<T> topStackEntry = stack.pop();
				if(previousPoped==null || topStackEntry.fibHeapEntry.parent == previousPoped.fibHeapEntry){
					res.addNodeImage(new FibHeapNodeImage(dx*rightmostFree, dy*topStackEntry.level, ((Node)topStackEntry.fibHeapEntry.getValue()).getId(), topStackEntry.fibHeapEntry.priority, Color.green, topStackEntry.fibHeapEntry.mark));
					topStackEntry.fibHeapEntry.x = dx*rightmostFree;
					topStackEntry.fibHeapEntry.y = dy*topStackEntry.level;
				}else{
					rightmostFree++;
					res.addNodeImage(new FibHeapNodeImage(dx*rightmostFree, dy*topStackEntry.level, ((Node)topStackEntry.fibHeapEntry.getValue()).getId(), topStackEntry.fibHeapEntry.priority, Color.green, topStackEntry.fibHeapEntry.mark));
					topStackEntry.fibHeapEntry.x = dx*rightmostFree;
					topStackEntry.fibHeapEntry.y = dy*topStackEntry.level;
				}
				
				if(topStackEntry.fibHeapEntry.child != null){
					FibHeapEntry<T> firstChildEntry = topStackEntry.fibHeapEntry.child;
					FibHeapEntry<T> curChildEntry = firstChildEntry;
					while(true){
						curChildEntry = curChildEntry.left;
						stack.push(new StackEntry<T>(curChildEntry, topStackEntry.level+1));
						if(curChildEntry==firstChildEntry) break;
					}
				}
				previousPoped = topStackEntry;
			}
			addLines(minEntry, res, true);
			
			return res;
		}
}
