import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class Visualizer extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;

	private int currentState = 0;
	private ArrayList<GraphState> graphStates;
	private ArrayList<FibHeapState> fibHeapStates;
	
	private JPanel topPanel;
	private JPanel centerPanel;
	private JPanel bottomPanel;
	
	private JPanel graphPanel;
	private JPanel fibHeapPanel;
	
	private JButton browseButton;
	private JButton runButton;
	private JTextField filePathField;
	
	private JFileChooser fileChooser;
	private String selectedFilePath = new String();
	
	private JButton nextStateButton;
	private JButton previousStateButton;
	private JButton firstStateButton;
	private JButton lastStateButton;
	
	public Visualizer(){
		super("Prim Algorithm - Fibonacci Heap");
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		constructFrame();		
	}
	
	private void constructFrame(){
		this.setSize(800, 600);
		this.setLayout(new BorderLayout());
		
		fileChooser = new JFileChooser(System.getProperty("user.dir"));
		
		topPanel = new JPanel();
		centerPanel = new JPanel();
		bottomPanel = new JPanel();
		
		//topPanel
		topPanel.setLayout(new FlowLayout());
		filePathField = new JTextField(25);
		filePathField.setEnabled(false);
		browseButton = new JButton("Browse...");
		runButton = new JButton("Run");
		runButton.setEnabled(false);
		browseButton.addActionListener(this);
		runButton.addActionListener(this);
		topPanel.add(filePathField);
		topPanel.add(browseButton);
		topPanel.add(runButton);
		
		//centerPanel
		centerPanel.setLayout(new GridLayout(1, 2));
		graphPanel = new JPanel();
		graphPanel.setLayout(new BorderLayout());
		fibHeapPanel = new JPanel();
		fibHeapPanel.setLayout(new BorderLayout());
		centerPanel.add(graphPanel);
		centerPanel.add(fibHeapPanel);
		
		//bottomPanel
		bottomPanel.setLayout(new FlowLayout());
		nextStateButton = new JButton("next");
		previousStateButton = new JButton("previous");
		firstStateButton = new JButton("first");
		lastStateButton = new JButton("last");
		nextStateButton.setEnabled(false);
		previousStateButton.setEnabled(false);
		firstStateButton.setEnabled(false);
		lastStateButton.setEnabled(false);
		nextStateButton.addActionListener(this);
		previousStateButton.addActionListener(this);
		firstStateButton.addActionListener(this);
		lastStateButton.addActionListener(this);
		bottomPanel.add(firstStateButton);
		bottomPanel.add(previousStateButton);
		bottomPanel.add(nextStateButton);
		bottomPanel.add(lastStateButton);
		
		
		this.add(topPanel, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	private void runFileChooser(){
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int response = fileChooser.showOpenDialog(this);
		if(response == JFileChooser.APPROVE_OPTION){
			selectedFilePath = fileChooser.getSelectedFile().toString();
			filePathField.setText(selectedFilePath);
		}
	}
	
	private void showState(int stateId){
		graphPanel.removeAll();
		JScrollPane graphScrollPane = new JScrollPane(graphStates.get(stateId));
		fibHeapPanel.add(graphScrollPane, BorderLayout.CENTER);
		graphPanel.add(graphScrollPane, BorderLayout.CENTER);
		JTextField graphPanelCommentary = new JTextField(graphStates.get(stateId).getCommentary());
		graphPanelCommentary.setEditable(false);
		graphPanel.add(graphPanelCommentary, BorderLayout.NORTH);
		graphPanel.revalidate();
		graphPanel.repaint();
		
		fibHeapPanel.removeAll();	
		JScrollPane fibHeapScrollPane = new JScrollPane(fibHeapStates.get(stateId));
		fibHeapPanel.add(fibHeapScrollPane, BorderLayout.CENTER);
		JTextField fibHeapPanelCommentary = new JTextField(fibHeapStates.get(stateId).getCommentary());
		fibHeapPanelCommentary.setEditable(false);
		fibHeapPanel.add(fibHeapPanelCommentary, BorderLayout.NORTH);
		fibHeapPanel.revalidate();
		fibHeapPanel.repaint();
		
		if(stateId==0){
			nextStateButton.setEnabled(true);
			previousStateButton.setEnabled(false);
			firstStateButton.setEnabled(false);
			lastStateButton.setEnabled(true);
		}else if(stateId==graphStates.size()-1){
			nextStateButton.setEnabled(false);
			previousStateButton.setEnabled(true);
			firstStateButton.setEnabled(true);
			lastStateButton.setEnabled(false);
		}else{
			nextStateButton.setEnabled(true);
			previousStateButton.setEnabled(true);
			firstStateButton.setEnabled(true);
			lastStateButton.setEnabled(true);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		
		if(ae.getActionCommand().equals(browseButton.getText())){
			runFileChooser();
			runButton.setEnabled(true);
		}else if(ae.getActionCommand().equals(runButton.getText())){
			Graph graph = new Graph();
			
			try {
				graph.readGraphFromFile(selectedFilePath);
				if(graph.isConnected()){
				
					PrimAlgorithm primAlg = new PrimAlgorithm();
					primAlg.mstPrim(graph);
					
					nextStateButton.setEnabled(true);
					previousStateButton.setEnabled(true);
					firstStateButton.setEnabled(true);
					lastStateButton.setEnabled(true);
					
					graphStates = primAlg.getGraphStates();
					fibHeapStates = primAlg.getFibHeapStates();
					
					System.out.println(graphStates.size() + " " + fibHeapStates.size());
					
					currentState = 0;
					showState(currentState);
				}else{
					JOptionPane.showMessageDialog(null,
						    "Graph is not connected.");
				}
			} catch (InputMismatchException e) {
				JOptionPane.showMessageDialog(null,
					    "Incorrect input file.");
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null,
					    "File not found.");
			} catch (NoSuchElementException e) {
				JOptionPane.showMessageDialog(null,
					    "Incorrect input file.");
			}
			
			
		}else if(ae.getActionCommand().equals(nextStateButton.getText())){
			currentState++;
			showState(currentState);
		}else if(ae.getActionCommand().equals(previousStateButton.getText())){
			currentState--;
			showState(currentState);
		}else if(ae.getActionCommand().equals(firstStateButton.getText())){
			currentState=0;
			showState(currentState);
		}else if(ae.getActionCommand().equals(lastStateButton.getText())){	
			currentState=graphStates.size()-1;
			showState(currentState);
		}
	}

	
}
