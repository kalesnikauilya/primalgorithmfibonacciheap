import java.awt.Color;

public class FibHeapEdgeImage {
	public int x1;
	public int y1;
	public int x2;
	public int y2;
	public Color color;
		
	public int nodeId1;
	public int nodeId2;
	public FibHeapEdgeImage(int xx1, int yy1, int xx2, int yy2, int id1, int id2, Color cc){
		x1=xx1;
		y1=yy1;
		x2=xx2;
		y2=yy2;
		nodeId1 = id1;
		nodeId2 = id2;
		color=cc;
	}
}

