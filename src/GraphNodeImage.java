import java.awt.Color;

public class GraphNodeImage {
	public int x;
	public int y;
	public int id;
	public Color color;

	public GraphNodeImage(int xx, int yy, int idd, Color cc){
		x=xx;
		y=yy;
		id=idd;
		color=cc;
	}
}
